import br.ufg.pos.fswm.fpb.generated.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 19/06/17.
 */
public class Principal {

    public static void main(String... args) {
        XigniteCurrencies gerador = new XigniteCurrencies();
        XigniteCurrenciesSoap soap = gerador.getXigniteCurrenciesSoap();

        ExchangeConversion conversion = soap.convertRealTimeValue(Currencies.USD.value(), Currencies.BRL.value(), 1.0);

        final String from = conversion.getFrom().getName();
        final String to = conversion.getTo().getName();
        final double amount = conversion.getAmount();
        final double rate = conversion.getExchangeRate();
        final double result = conversion.getResult();

        System.out.println("O valor " + amount + " corresponde a conversão de '" + from + "' para '" + to + "' a uma taxa de " + rate + " resultando  em  " + result);
    }
}
